// ==UserScript==
// @name         bling.js
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       Macsy
// @match        *://*/*
// @grant        none
// ==/UserScript==

(function() {
    function addGlobalStyle(css) {
        var head, style;
        head = document.getElementsByTagName('head')[0];
        if (!head) { return; }
        style = document.createElement('style');
        style.type = 'text/css';
        style.innerHTML = css;
        head.appendChild(style);
    }
    addGlobalStyle('html,body,h1,h2,h3,h4,h5,p,li,a{font-family: "Segoe UI", Tahoma, -apple-system, BlinkMacSystemFont, sans-serif !important;}');
})();